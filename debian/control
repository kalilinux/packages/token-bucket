Source: token-bucket
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest,
               python3-pytest-runner,
               python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/falconry/token-bucket
Vcs-Browser: https://gitlab.com/kalilinux/packages/token-bucket
Vcs-Git: https://gitlab.com/kalilinux/packages/token-bucket.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-token-bucket
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Token Bucket Implementation for Python Web Apps (Python 3)
 This package contains an implementation of the token bucket algorithm suitable
 for use in web applications for shaping or policing request rates. This
 implementation does not require the use of an independent timer thread to
 manage the bucket state.
 .
 Compared to other rate-limiting algorithms that use a simple counter, the token
 bucket algorithm provides the following advantages:
 .
 The thundering herd problem is avoided since bucket capacity is replenished
 gradually, rather than being immediately refilled at the beginning of each
 epoch as is common with simple fixed window counters.
 Burst duration can be explicitly controlled.
 .
 Moving window algorithms are resistant to bursting, but at the cost of
 additional processing and memory overhead vs. the token bucket algorithm which
 uses a simple, fast counter per key. The latter approach does allow for
 bursting, but only for a controlled duration.
 .
 This package installs the library for Python 3.
